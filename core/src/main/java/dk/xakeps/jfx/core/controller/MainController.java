/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.core.TabButton;
import dk.xakeps.jfx.core.view.mainview.CollectMainViewTabsEvent;
import dk.xakeps.jfx.core.view.mainview.MainViewTab;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;

import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

public class MainController extends AbstractController implements Initializable {
    private final ToggleGroup tabsPaneToggleGroup = new ToggleGroup();
    @FXML
    private BorderPane borderPane;
    @FXML
    private FlowPane tabsButtonsPane;

    public void onServerSelect(ActionEvent actionEvent) {
        Button source = (Button) actionEvent.getSource();
        for (Node node : source.getParent().getChildrenUnmodifiable()) {
            node.getStyleClass().remove("server-button-selected");
        }
        source.getStyleClass().add("server-button-selected");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        CollectMainViewTabsEvent event = new CollectMainViewTabsEvent();
        getEventBus().fireEvent(event);
        List<MainViewTab> tabs = event.getTabs();
        for (MainViewTab tab : tabs) {
            String tabName = tab.getView()
                    .getBundle().map(b -> b.getString(tab.getId()))
                    .orElse(tab.getId());
            TabButton tabButton = new TabButton();
            tabButton.setToggleGroup(tabsPaneToggleGroup);
            tabsButtonsPane.getChildren().add(tabButton);
            tabButton.setText(tabName);
            tabButton.setOnAction(event1 -> {
                borderPane.setCenter(tab.getView().getContent());
            });
        }
        Iterator<Toggle> iterator = tabsPaneToggleGroup.getToggles().iterator();
        if (iterator.hasNext()) {
            TabButton next = (TabButton) iterator.next();
            next.fire();
        }

        Label loginId = new Label("Unauthorized");
        tabsButtonsPane.getChildren()
                .add(loginId);
        if (getProfileManager() != null) {
            loginId.setText(getProfileManager().getSelectedProfile().getName());
        }
        profileManagerProperty().addListener((observable, oldValue, newValue) -> {
            loginId.setText(newValue.getSelectedProfile().getName());
        });
    }
}
