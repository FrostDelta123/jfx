/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.profile;

import com.fasterxml.jackson.core.type.TypeReference;
import dk.xakeps.jfx.core.DirectoryManager;
import dk.xakeps.jfx.core.auth.model.AccountModel;
import dk.xakeps.jfx.core.profile.dto.LauncherProfileModelDto;
import dk.xakeps.jfx.core.profile.dto.LauncherProfilesDto;
import dk.xakeps.jfx.core.settings.Settings;
import dk.xakeps.jfx.core.settings.SettingsLoader;
import dk.xakeps.jfx.core.settings.impl.SettingsLoaderImpl;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProfileManagerFactory {
    private final SettingsLoader settingsLoader;

    public ProfileManagerFactory(DirectoryManager directoryManager) {
        this.settingsLoader = new SettingsLoaderImpl(directoryManager.getConfigDirectory().resolve("profiles"));
    }

    public ProfileManager createProfileManager(AccountModel accountModel) {
        Settings<LauncherProfilesDto> settings = getSettings(accountModel);
        LauncherProfilesDto dto = settings.getObject();
        List<LauncherProfileModel> profiles = dto.getProfiles().stream()
                .map(LauncherProfileModelDto::asModel)
                .collect(Collectors.toList());
        LauncherProfileModel selectedProfile = profiles.stream().filter(m -> m.getName().equals(dto.getSelectedProfile())).findFirst()
                .orElse(null);
        if (selectedProfile == null) {
            selectedProfile = new LauncherProfileModel();
            selectedProfile.setName(accountModel.getSelectedProfile().getUserName());
            profiles.add(selectedProfile);
        }
        ProfileManager profileManager = new ProfileManager(accountModel, this);
        profileManager.getProfiles().addAll(profiles);
        profileManager.setSelectedProfile(selectedProfile);
        return profileManager;
    }

    void saveProfileManager(ProfileManager profileManager) throws IOException {
        LauncherProfilesDto dto = new LauncherProfilesDto(new ArrayList<>(), profileManager.getSelectedProfile().getName());
        for (LauncherProfileModel profile : profileManager.getProfiles()) {
            dto.getProfiles().add(new LauncherProfileModelDto(profile.getName(), profile.getProfileDirectory()));
        }

        Settings<LauncherProfilesDto> load = settingsLoader.load(
                Paths.get(profileManager.getAccountModel().getSelectedProfile().getUserName()),
                dto,
                new TypeReference<LauncherProfilesDto>() {}
        );
        load.save();
    }

    private Settings<LauncherProfilesDto> getSettings(AccountModel accountModel) {
        Settings<LauncherProfilesDto> load = settingsLoader.load(
                Paths.get(accountModel.getSelectedProfile().getUserName()),
                new LauncherProfilesDto(),
                new TypeReference<LauncherProfilesDto>() {}
        );
        try {
            load.reload();
        } catch (IOException e) {
            // ignored
        }
        return load;
    }
}
