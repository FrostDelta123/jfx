/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.app;

import dk.xakeps.jfx.core.DirectoryManager;
import dk.xakeps.jfx.core.GUIControllerFactory;
import dk.xakeps.jfx.core.auth.AuthModule;
import dk.xakeps.jfx.core.event.EventBus;
import dk.xakeps.jfx.core.profile.ProfileManager;
import dk.xakeps.jfx.core.profile.ProfileManagerFactory;
import dk.xakeps.jfx.core.settings.SettingsLoader;
import dk.xakeps.jfx.core.settings.impl.SettingsLoaderImpl;
import dk.xakeps.jfx.core.view.ViewManager;
import dk.xakeps.jfx.core.view.Views;
import dk.xakeps.jfx.core.view.mainview.CollectMainViewTabsEvent;
import javafx.application.Application;
import javafx.application.Preloader;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ServiceLoader;
import java.util.prefs.Preferences;

public class Main extends Application {
    private static final String LAUNCHER_ID = "jfx.launcher";
    private static final ObjectProperty<ViewManager> VIEW_MANAGER = new SimpleObjectProperty<>();
    private static final ObjectProperty<EventBus> EVENT_BUS = new SimpleObjectProperty<>();
    private static final ObjectProperty<ProfileManager> PROFILE_MANAGER = new SimpleObjectProperty<>();
    public static final GUIControllerFactory CONTROLLER_FACTORY = new GUIControllerFactory(VIEW_MANAGER, EVENT_BUS, PROFILE_MANAGER);

//    static {
//        System.setProperty("javafx.preloader", AppPreloader.class.getName());
//    }

    public static void main(String[] args) {
        Application.launch(Main.class, args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        // todo: per platform search, linux - xdg, mac - app data, windows - app data
        String directory = Preferences.userNodeForPackage(getClass()).node(LAUNCHER_ID).get("directory", null);
        if (directory == null) {
            directory = Paths.get(System.getProperty("user.home"), LAUNCHER_ID).toString();
        }
        Path path = Paths.get(directory);
        DirectoryManager directoryManager = new DirectoryManager(path);

        stage.getIcons().add(new Image(getClass().getResourceAsStream("/logo.png")));

        EVENT_BUS.set(new EventBus());
        EVENT_BUS.get().addListener(CollectMainViewTabsEvent.class, event -> {
            event.addTab(new SimpleTextTab("First tab"));
            event.addTab(new SimpleTextTab("Second tab"));
        });

        AuthModule authModule = ServiceLoader.load(AuthModule.class).findFirst().orElseThrow();
        String name = authModule.getClass().getModule().getName();
        // todo: handle better
        if (name == null) {
            name = authModule.getClass().getName();
        }
        SettingsLoader loader = new SettingsLoaderImpl(Paths.get("launcher", "configs", name));
        authModule.setSettingsLoader(loader);

        ViewManager viewManager = new ViewManager(stage);
        VIEW_MANAGER.set(viewManager);
        viewManager.setCurrentView(authModule.getView());
        notifyPreloader(new Preloader.PreloaderNotification() {});
        stage.show();

        authModule.accountModelProperty().addListener((observable, oldValue, newValue) -> {
            ProfileManagerFactory factory = new ProfileManagerFactory(directoryManager);
            ProfileManager profileManager = factory.createProfileManager(newValue);
            try {
                profileManager.save();
            } catch (IOException e) {
                e.printStackTrace();
            }
            PROFILE_MANAGER.set(profileManager);
            viewManager.setCurrentView(Views.MAIN_VIEW);
            System.out.println(newValue); // TODO: remove debug
        });
    }
}
