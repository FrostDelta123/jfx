/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.profile.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class LauncherProfilesDto {
    private final List<LauncherProfileModelDto> profiles;
    private final String selectedProfile;

    public LauncherProfilesDto() {
        this.profiles = Collections.emptyList();
        this.selectedProfile = "";
    }

    @JsonCreator
    public LauncherProfilesDto(@JsonProperty("profiles") List<LauncherProfileModelDto> profiles,
                               @JsonProperty("selectedProfile") String selectedProfile) {
        this.profiles = Objects.requireNonNullElse(profiles, Collections.emptyList());
        this.selectedProfile = Objects.requireNonNullElse(selectedProfile, "");
    }

    public List<LauncherProfileModelDto> getProfiles() {
        return profiles;
    }

    public String getSelectedProfile() {
        return selectedProfile;
    }
}
