package dk.xakeps.jfx.core.settings;

public interface SettingsConsumer {
    void setSettingsLoader(SettingsLoader settingsLoader);
}
