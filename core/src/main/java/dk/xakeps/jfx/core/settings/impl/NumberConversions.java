/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SoKnight

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package dk.xakeps.jfx.core.settings.impl;

class NumberConversions {

    /*
     * 'String -> ?' converters.
     */
    
    public static Integer getAsInteger(String source) {
        try {
            return Integer.parseInt(source);
        } catch (NumberFormatException ignored) {
            return null;
        }
    }
    
    public static Long getAsLong(String source) {
        try {
            return Long.parseLong(source);
        } catch (NumberFormatException ignored) {
            return null;
        }
    }
    
    /*
     * Associated validators.
     */
    
    public static boolean isInteger(String source) {
        return getAsInteger(source) != null;
    }
    
    public static boolean isLong(String source) {
        return getAsLong(source) != null;
    }
    
}

