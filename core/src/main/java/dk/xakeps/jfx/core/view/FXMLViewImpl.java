/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.view;

import dk.xakeps.jfx.core.app.Main;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.ResourceBundle;

public class FXMLViewImpl implements FXMLView {
    private final URL viewUrl;
    protected FXMLLoader loader;

    protected Object controller;
    protected Node content;

    public FXMLViewImpl(URL viewUrl) {

        this.loader = new FXMLLoader(viewUrl);
        String lang = viewUrl.toString().replaceAll(".fxml", "");
        URL[] urls = {viewUrl};
        ClassLoader classLoader = new URLClassLoader(urls);
        ResourceBundle bundle = ResourceBundle.getBundle(lang.substring(lang.lastIndexOf("/") + 1), Locale.getDefault(), classLoader);

        loader.setResources(bundle);

        loader.setCharset(StandardCharsets.UTF_8);
        loader.setControllerFactory(Main.CONTROLLER_FACTORY);
        this.viewUrl = viewUrl;

    }

    @Override
    public Object getController() {
        ensureLoaded();
        return controller;
    }

    @Override
    public Node getContent() {
        ensureLoaded();
        return content;
    }

    @Override
    public boolean isLoaded() {
        return loader == null;
    }

    @Override
    public void load() {
        try {
            content = loader.load();
            controller = loader.getController();
            loader = null;
        } catch (IOException e) {
            throw new ViewLoadException(
                    String.format("Can't load view. Class: %s, URL: %s", getClass().getName(), viewUrl),
                    e
            );
        }
    }

    private void ensureLoaded() {
        if (!isLoaded()) {
            load();
        }
    }
}
