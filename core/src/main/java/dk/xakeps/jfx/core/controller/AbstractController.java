/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.core.event.EventBus;
import dk.xakeps.jfx.core.profile.ProfileManager;
import dk.xakeps.jfx.core.view.ViewManager;
import javafx.beans.value.ObservableValue;

public class AbstractController {
    private ObservableValue<ViewManager> viewManager;
    private ObservableValue<EventBus> eventBus;
    private ObservableValue<ProfileManager> profileManager;

    public void setViewManagerProperty(ObservableValue<ViewManager> viewManager) {
        this.viewManager = viewManager;
    }

    public ViewManager getViewManager() {
        return viewManager.getValue();
    }

    public ObservableValue<ViewManager> viewManagerProperty() {
        return viewManager;
    }

    public void setEventBusProperty(ObservableValue<EventBus> eventBus) {
        this.eventBus = eventBus;
    }

    public EventBus getEventBus() {
        return eventBus.getValue();
    }

    public ObservableValue<EventBus> eventBusProperty() {
        return eventBus;
    }

    public void setProfileManagerProperty(ObservableValue<ProfileManager> profileManager) {
        this.profileManager = profileManager;
    }

    public ProfileManager getProfileManager() {
        return profileManager.getValue();
    }

    public ObservableValue<ProfileManager> profileManagerProperty() {
        return profileManager;
    }
}
