/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package dk.xakeps.jfx.core.profile;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.nio.file.Path;

public class LauncherProfileModel {
    private final StringProperty name = new SimpleStringProperty(this, "name");
    private final ObjectProperty<Path> profileDirectory = new SimpleObjectProperty<>(this, "profileDirectory");

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public Path getProfileDirectory() {
        return profileDirectory.get();
    }

    public ObjectProperty<Path> profileDirectoryProperty() {
        return profileDirectory;
    }

    public void setProfileDirectory(Path profileDirectory) {
        this.profileDirectory.set(profileDirectory);
    }
}
