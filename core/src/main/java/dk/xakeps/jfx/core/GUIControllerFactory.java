/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core;

import dk.xakeps.jfx.core.controller.AbstractController;
import dk.xakeps.jfx.core.event.EventBus;
import dk.xakeps.jfx.core.profile.ProfileManager;
import dk.xakeps.jfx.core.view.ViewManager;
import javafx.beans.value.ObservableValue;
import javafx.util.Callback;

import java.lang.reflect.InvocationTargetException;

public class GUIControllerFactory implements Callback<Class<?>, Object> {
    private final ObservableValue<ViewManager> viewManager;
    private final ObservableValue<EventBus> eventBus;
    private final ObservableValue<ProfileManager> profileManager;

    public GUIControllerFactory(ObservableValue<ViewManager> viewManager,
                                ObservableValue<EventBus> eventBus,
                                ObservableValue<ProfileManager> profileManager) {
        this.viewManager = viewManager;
        this.eventBus = eventBus;
        this.profileManager = profileManager;
    }

    @Override
    public Object call(Class<?> param) {
        try {
            Object o = param.getDeclaredConstructor().newInstance();
            if (o instanceof AbstractController) {
                ((AbstractController) o).setViewManagerProperty(viewManager);
                ((AbstractController) o).setEventBusProperty(eventBus);
                ((AbstractController) o).setProfileManagerProperty(profileManager);
            }
            return o;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new ControllerInstantiationException(e);
        }
    }
}
