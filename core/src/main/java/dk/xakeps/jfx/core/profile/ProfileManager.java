/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package dk.xakeps.jfx.core.profile;

import dk.xakeps.jfx.core.auth.model.AccountModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;

public class ProfileManager {
    private final AccountModel accountModel;
    private final ObservableList<LauncherProfileModel> profiles = FXCollections.observableArrayList();
    private final ObjectProperty<LauncherProfileModel> selectedProfile = new SimpleObjectProperty<>(this, "selectedProfile");

    private final ProfileManagerFactory factory;

    public ProfileManager(AccountModel accountModel, ProfileManagerFactory factory) {
        this.accountModel = accountModel;
        this.factory = factory;
    }

    public AccountModel getAccountModel() {
        return accountModel;
    }

    public ObservableList<LauncherProfileModel> getProfiles() {
        return profiles;
    }

    public LauncherProfileModel getSelectedProfile() {
        return selectedProfile.get();
    }

    public ObjectProperty<LauncherProfileModel> selectedProfileProperty() {
        return selectedProfile;
    }

    public void setSelectedProfile(LauncherProfileModel selectedProfile) {
        this.selectedProfile.set(selectedProfile);
    }

    public void save() throws IOException {
        factory.saveProfileManager(this);
    }
}
