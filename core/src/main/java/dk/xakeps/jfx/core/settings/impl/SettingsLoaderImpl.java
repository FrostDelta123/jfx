/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.settings.impl;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import dk.xakeps.jfx.core.settings.Settings;
import dk.xakeps.jfx.core.settings.SettingsLoader;
import dk.xakeps.jfx.core.settings.SettingsPathException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class SettingsLoaderImpl implements SettingsLoader {
    private final Path root;
    private final ObjectMapper objectMapper;

    public SettingsLoaderImpl(Path root) {
        this.root = root;
        this.objectMapper = new ObjectMapper()
                .enable(SerializationFeature.INDENT_OUTPUT)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    @Override
    public <T> Settings<T> load(Path path, T def, TypeReference<T> typeReference) {
        Objects.requireNonNull(path, "The received path is null.");

        Path cfgDir;
        if (path.getNameCount() > 1) {
            cfgDir = root.resolve(path.getParent());
        } else {
            cfgDir = root;
        }
        Path file = cfgDir.resolve(path.getFileName().toString() + ".json");
        if (!file.startsWith(root)) {
            throw new SettingsPathException(String.format(
                    "Wrong setting path provided. Root: %s, Path: %s, Resolved: %s", root, path, file
            ));
        }

        return new SettingsImpl<>(file, objectMapper, typeReference, def);
    }

    private static class SettingsImpl<T> implements Settings<T> {
        private final Path file;
        private final ObjectMapper objectMapper;
        private final TypeReference<T> typeReference;
        private T object;
        
        private SettingsImpl(Path file, ObjectMapper objectMapper, TypeReference<T> typeReference, T object) {
            this.file = file;
            this.objectMapper = objectMapper;
            this.typeReference = typeReference;
            this.object = object;
        }

        @Override
        public void reload() throws IOException {
            if (Files.notExists(file) || Files.size(file) == 0) {
                return;
            }
            try(BufferedReader reader = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
                object = objectMapper.readValue(reader, typeReference);
            }
        }

        @Override
        public void remove() throws IOException {
            Files.deleteIfExists(file);
        }

        @Override
        public T getObject() {
            return object;
        }

        @Override
        public void setObject(T object) {
            this.object = object;
        }

        @Override
        public void save() throws IOException {
            if (object == null) {
                return;
            }
            if (Files.notExists(file.getParent())) {
                Files.createDirectories(file.getParent());
            }
            try(BufferedWriter writer = Files.newBufferedWriter(file, StandardCharsets.UTF_8)) {
                objectMapper.writeValue(writer, object);
            }
        }

    }

    
}
