/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.auth.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class AccountModel {

    private final String email;
    private final String accessToken;
    private final String clientToken;

    private final List<AccountProfileModel> availableProfiles;
    private final AccountProfileModel selectedProfile;

    @JsonCreator
    public AccountModel(@JsonProperty("email") String email,
                        @JsonProperty("accessToken") String accessToken,
                        @JsonProperty("clientToken") String clientToken,
                        @JsonProperty("availableProfiles") List<AccountProfileModel> availableProfiles,
                        @JsonProperty("selectedProfile") AccountProfileModel selectedProfile) {
        this.email = email;
        this.accessToken = accessToken;
        this.clientToken = clientToken;
        this.availableProfiles = availableProfiles;
        this.selectedProfile = selectedProfile;
    }

    public String getEmail() {
        return email;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    public List<AccountProfileModel> getAvailableProfiles() {
        return availableProfiles;
    }

    public AccountProfileModel getSelectedProfile() {
        return selectedProfile;
    }

    @Override
    public String toString() {
        return "AccountModel{" +
                "email='" + email + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", clientToken='" + clientToken + '\'' +
                ", availableProfiles=" + availableProfiles +
                ", selectedProfile=" + selectedProfile +
                '}';
    }
}
