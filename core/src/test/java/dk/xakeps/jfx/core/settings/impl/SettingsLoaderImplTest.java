/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.settings.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import dk.xakeps.jfx.core.settings.Settings;
import dk.xakeps.jfx.core.settings.SettingsLoader;
import javafx.beans.property.StringProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class SettingsLoaderImplTest {

    private SettingsLoader loader;
    private Settings<TestConfig> config;

    @BeforeEach
    public void setup() throws IOException {
        Path root = Paths.get("").toAbsolutePath();
        loader = new SettingsLoaderImpl(root);
        config = loader.load(Paths.get("config"), new TestConfig(), new TypeReference<TestConfig>() {});
    }

    @Test
    public void basicConfigTest() throws IOException {
        config.reload();
        config.getObject().aLong = 32L;
        config.getObject().string = "string";
        config.save();
        setup();
        config.reload();
        assertEquals(32L, config.getObject().aLong);
        assertEquals("string", config.getObject().string);
    }

    public static class TestConfig {
        public String string;
        public Long aLong;

        public String getString() {
            return string;
        }

        public void setString(String string) {
            this.string = string;
        }

        public Long getaLong() {
            return aLong;
        }

        public void setaLong(Long aLong) {
            this.aLong = aLong;
        }
    }
}