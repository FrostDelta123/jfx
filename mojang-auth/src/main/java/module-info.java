/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import dk.xakeps.jfx.auth.mojang.MojangAuthModule;
import dk.xakeps.jfx.core.auth.AuthModule;

module dk.xakeps.jfx.auth.mojang {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.net.http;

    requires com.fasterxml.jackson.databind;

    requires dk.xakeps.jfx.core;

    provides AuthModule with MojangAuthModule;

    opens dk.xakeps.jfx.auth.mojang.controller to dk.xakeps.jfx.core, javafx.fxml;
    opens dk.xakeps.jfx.auth.mojang.data.request to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.auth.mojang.data.response to com.fasterxml.jackson.databind;
}