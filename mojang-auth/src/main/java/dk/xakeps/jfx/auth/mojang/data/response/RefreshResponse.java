/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.auth.mojang.data.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RefreshResponse {

    private final String accessToken;
    private final String clientToken;
    private final AuthResponse.User user;

    @JsonCreator
    public RefreshResponse(@JsonProperty("accessToken") String accessToken,
                           @JsonProperty("clientToken") String clientToken,
                           @JsonProperty("user") AuthResponse.User user) {
        this.accessToken = accessToken;
        this.clientToken = clientToken;
        this.user = user;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    public AuthResponse.User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "RefreshResponse{" +
                "accessToken='" + accessToken + '\'' +
                ", clientToken='" + clientToken + '\'' +
                ", user=" + user +
                '}';
    }
}
