/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.auth.mojang.controller;

import dk.xakeps.jfx.auth.mojang.MojangAuthModule;
import dk.xakeps.jfx.auth.mojang.exception.MojangApiErrorException;
import dk.xakeps.jfx.auth.mojang.service.YggdrasilService;
import dk.xakeps.jfx.auth.mojang.service.YggdrasilServiceImpl;
import dk.xakeps.jfx.core.auth.model.AccountModel;
import dk.xakeps.jfx.core.controller.AbstractController;
import dk.xakeps.jfx.auth.mojang.model.UserModel;
import dk.xakeps.jfx.core.exception.AuthenticatorException;
import dk.xakeps.jfx.core.overlay.IndicatorAndLabelOverlay;
import dk.xakeps.jfx.core.settings.Settings;
import javafx.application.Platform;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;

public class LoginController extends AbstractController implements Initializable {

    private final UserModel userModel = new UserModel();

    private final YggdrasilService yggdrasilService = new YggdrasilServiceImpl();

    private MojangAuthModule authModuleImpl;

    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private CheckBox rememberMeCheckBox;

    public void onLogIn(ActionEvent event) {

        if (!validateInput(usernameField, passwordField)) {
            return;
        }

        IndicatorAndLabelOverlay overlay = new IndicatorAndLabelOverlay();
        Label label = overlay.getLabel();

        label.setText("Loading...");

        CompletableFuture<AccountModel> authFuture = yggdrasilService.authenticate(userModel).whenCompleteAsync((accountModel, throwable) -> {
            if (throwable instanceof MojangApiErrorException) {
                MojangApiErrorException exception =  (MojangApiErrorException) throwable;

                // TODO: show overlay longer
                label.setText(exception.getErrorMessage());
                label.setTextFill(Color.RED);
                return;
            }
            if (throwable instanceof AuthenticatorException) {
                throwable.printStackTrace();
                label.setText("Undefined authenticator error");
                label.setTextFill(Color.RED);
                return;
            }
            authModuleImpl.setAccountModel(accountModel, rememberMeCheckBox.isSelected());
        }, Platform::runLater);

        getViewManager().runWithOverlay(overlay, authFuture);
    }

    public UserModel getUserModel() {
        return userModel;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usernameField.textProperty().bindBidirectional(userModel.usernameProperty());
        passwordField.textProperty().bindBidirectional(userModel.passwordProperty());
        rememberMeCheckBox.selectedProperty().bindBidirectional(userModel.rememberMeProperty());

        addValidationListener(usernameField, passwordField);

        final Settings<AccountModel> loginData = authModuleImpl.getLoginData();
        final AccountModel accountModel = loginData.getObject();

        if (accountModel != null) {
            CompletableFuture<Boolean> validate = yggdrasilService.validate(accountModel);

            validate.whenCompleteAsync((result, throwable) -> {
                if (!result) {
                    yggdrasilService.refresh(accountModel).whenComplete((newAccountModel, throwable1) -> {
                        authModuleImpl.setAccountModel(newAccountModel, true);
                    });
                } else {
                    authModuleImpl.setAccountModel(accountModel, false);
                }
            }, Platform::runLater);
        }
    }

    private boolean validateInput(TextField... fields) {
        boolean valid = true;

        for (TextField field : fields) {
            final String inputText = field.getText();
            PseudoClass error = PseudoClass.getPseudoClass("error");
            if (inputText == null || inputText.isEmpty()) {
                field.pseudoClassStateChanged(error, true);
                valid = false;
            } else {
                field.pseudoClassStateChanged(error, false);
            }
        }

        return valid;
    }

    private void addValidationListener(TextField... fields) {
        for (TextField field : fields) {
            field.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) { //unfocus
                    validateInput(field);
                }
            });
        }
    }

    public void setAuthModuleImpl(MojangAuthModule authModuleImpl) {
        this.authModuleImpl = authModuleImpl;
    }

    public void onMousePressedTextField(MouseEvent mouseEvent) {
        Node source = (Node) mouseEvent.getSource();
        source.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), false);
    }
}
