/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.auth.mojang.data.request;

public class AuthRequest {

    private final Agent agent;
    private final String username;
    private final String password;
    private final String clientToken;
    private final boolean requestUser;

    public AuthRequest(Agent agent, String username, String password, String clientToken, boolean requestUser) {
        this.agent = agent;
        this.username = username;
        this.password = password;
        this.clientToken = clientToken;
        this.requestUser = requestUser;
    }

    public AuthRequest(String username, String password, String clientToken, boolean requestUser) {
        this(Agent.MINECRAFT, username, password, clientToken, requestUser);
    }

    public Agent getAgent() {
        return agent;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getClientToken() {
        return clientToken;
    }

    public boolean isRequestUser() {
        return requestUser;
    }

    public static class Agent {

        public static final Agent MINECRAFT = new Agent("Minecraft", 1);

        private final String name;
        private final int version;

        public Agent(String name, int version) {
            this.name = name;
            this.version = version;
        }

        public String getName() {
            return name;
        }

        public int getVersion() {
            return version;
        }
    }
}
