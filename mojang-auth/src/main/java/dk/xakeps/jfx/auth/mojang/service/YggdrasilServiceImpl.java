/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.auth.mojang.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.auth.mojang.data.request.AuthRequest;
import dk.xakeps.jfx.auth.mojang.data.request.InvalidateRequest;
import dk.xakeps.jfx.auth.mojang.data.request.RefreshRequest;
import dk.xakeps.jfx.auth.mojang.data.request.ValidateRequest;
import dk.xakeps.jfx.auth.mojang.data.response.AuthResponse;
import dk.xakeps.jfx.auth.mojang.data.response.ErrorResponse;
import dk.xakeps.jfx.auth.mojang.data.response.RefreshResponse;
import dk.xakeps.jfx.auth.mojang.exception.MojangApiErrorException;
import dk.xakeps.jfx.auth.mojang.exception.NoSelectedProfileException;
import dk.xakeps.jfx.auth.mojang.model.UserModel;
import dk.xakeps.jfx.core.auth.model.AccountModel;
import dk.xakeps.jfx.core.auth.model.AccountProfileModel;
import dk.xakeps.jfx.core.exception.AuthenticatorException;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class YggdrasilServiceImpl implements YggdrasilService {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final HttpClient httpClient = HttpClient.newBuilder().build();

    @Override
    public CompletableFuture<AccountModel> authenticate(UserModel userModel) {
        return CompletableFuture.supplyAsync(() -> {
            AuthRequest authRequest = new AuthRequest(userModel.getUsername(), userModel.getPassword(), UUID.randomUUID().toString(), true);

            try {
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create("https://authserver.mojang.com/authenticate"))
                        .header("Content-Type", "application/json")
                        .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(authRequest)))
                        .build();

                HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

                AuthResponse authResponse = processMojangApiError(send, AuthResponse.class);

                final List<AccountProfileModel> availableProfiles = authResponse.getAvailableProfiles().stream()
                        .map(playerProfile -> new AccountProfileModel(playerProfile.getId(), playerProfile.getName()))
                        .collect(Collectors.toList());
                final AccountProfileModel selectedProfile = availableProfiles.stream()
                        .filter(accountProfileModel -> accountProfileModel.getUserId().equals(authResponse.getSelectedProfile().getId()))
                        .findFirst()
                        .orElseThrow(NoSelectedProfileException::new);

                return new AccountModel(
                        authResponse.getUser().getUsername(), authResponse.getAccessToken(),
                        authRequest.getClientToken(), availableProfiles, selectedProfile
                );

            } catch (IOException | InterruptedException ex) {
                throw new AuthenticatorException(ex);
            }
        });
    }

    @Override
    public CompletableFuture<AccountModel> refresh(AccountModel accountModel) {
        return CompletableFuture.supplyAsync(() -> {
            RefreshRequest refreshRequest = new RefreshRequest(accountModel.getAccessToken(), accountModel.getClientToken(), false);

            try {
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create("https://authserver.mojang.com/refresh"))
                        .header("Content-Type", "application/json")
                        .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(refreshRequest)))
                        .build();

                HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

                RefreshResponse refreshResponse = processMojangApiError(send, RefreshResponse.class);

                return new AccountModel(
                        accountModel.getEmail(), refreshResponse.getAccessToken(), refreshResponse.getClientToken(),
                        accountModel.getAvailableProfiles(), accountModel.getSelectedProfile()
                );
            } catch (IOException | InterruptedException ex) {
                throw new AuthenticatorException(ex);
            }
        });
    }

    @Override
    public CompletableFuture<Boolean> validate(AccountModel accountModel) {
        return CompletableFuture.supplyAsync(() -> {
            ValidateRequest validateRequest = new ValidateRequest(accountModel.getAccessToken(), accountModel.getClientToken());

            try {
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create("https://authserver.mojang.com/validate"))
                        .header("Content-Type", "application/json")
                        .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(validateRequest)))
                        .build();

                HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

                return !hasError(send);
            } catch (IOException | InterruptedException ex) {
                throw new AuthenticatorException(ex);
            }
        });
    }

    @Override
    public CompletableFuture<Boolean> invalidate(AccountModel accountModel) {
        return CompletableFuture.supplyAsync(() -> {
            InvalidateRequest validateRequest = new InvalidateRequest(accountModel.getAccessToken(), accountModel.getClientToken());

            try {
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create("https://authserver.mojang.com/invalidate"))
                        .header("Content-Type", "application/json")
                        .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(validateRequest)))
                        .build();

                HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

                return !hasError(send);
            } catch (IOException | InterruptedException ex) {
                throw new AuthenticatorException(ex);
            }
        });
    }

    private <T> T processMojangApiError(HttpResponse<String> response, Class<T> successResponseType) {
        try {
            hasError(response);

            return objectMapper.readValue(response.body(), successResponseType);
        } catch (JsonProcessingException e) {
            throw new AuthenticatorException(e);
        }
    }

    private boolean hasError(HttpResponse<String> response) {
        try {
            if (response.statusCode() / 100 != 2) {
                ErrorResponse errorResponse = objectMapper.readValue(response.body(), ErrorResponse.class);

                throw new MojangApiErrorException(
                        errorResponse.getError(),
                        errorResponse.getErrorMessage(),
                        errorResponse.getCause()
                );
            }
        } catch (JsonProcessingException e) {
            throw new AuthenticatorException(e);
        }
        return false;
    }
}
