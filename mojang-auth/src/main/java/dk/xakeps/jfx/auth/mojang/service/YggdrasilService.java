/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.auth.mojang.service;

import dk.xakeps.jfx.auth.mojang.model.UserModel;
import dk.xakeps.jfx.core.auth.model.AccountModel;

import java.util.concurrent.CompletableFuture;

public interface YggdrasilService {

    CompletableFuture<AccountModel> authenticate(UserModel userModel);

    CompletableFuture<AccountModel> refresh(AccountModel accountModel);

    CompletableFuture<Boolean> validate(AccountModel accountModel);

    CompletableFuture<Boolean> invalidate(AccountModel accountModel);

}
