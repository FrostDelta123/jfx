/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.auth.mojang.data.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthResponse {

    private final User user;
    private final String accessToken;
    private final String clientToken;
    private final List<PlayerProfile> availableProfiles;
    private final PlayerProfile selectedProfile;

    @JsonCreator
    public AuthResponse(@JsonProperty("user") User user,
                        @JsonProperty("accessToken") String accessToken,
                        @JsonProperty("clientToken") String clientToken,
                        @JsonProperty("availableProfiles") List<PlayerProfile> availableProfiles,
                        @JsonProperty("selectedProfile") PlayerProfile selectedProfile) {
        this.user = user;
        this.accessToken = accessToken;
        this.clientToken = clientToken;
        this.availableProfiles = Collections.unmodifiableList(availableProfiles);
        this.selectedProfile = selectedProfile;
    }

    public User getUser() {
        return user;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    public List<PlayerProfile> getAvailableProfiles() {
        return availableProfiles;
    }

    public PlayerProfile getSelectedProfile() {
        return selectedProfile;
    }

    @Override
    public String toString() {
        return "AuthResponse{" +
                "user=" + user +
                ", accessToken='" + accessToken + '\'' +
                ", clientToken='" + clientToken + '\'' +
                ", availableProfiles=" + availableProfiles +
                ", selectedProfile=" + selectedProfile +
                '}';
    }

    public static class PlayerProfile {
        private final String id;
        private final String name;

        /*private final String userId;
        private final long createdAt;
        private final boolean legacyProfile;
        private final boolean suspended;
        private final boolean paid;
        private final boolean migrated;
        private final boolean legacy;*/

        @JsonCreator
        public PlayerProfile(@JsonProperty("id") String id,
                             @JsonProperty("name") String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "PlayerProfile{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    public static class User {
        private final List<UserProperty> properties;
        private final String id;
        private final String username;

        @JsonCreator
        public User(@JsonProperty("properties") List<UserProperty> properties,
                    @JsonProperty("id") String id,
                    @JsonProperty("username") String username) {
            this.properties = properties;
            this.id = id;
            this.username = username;
        }

        public List<UserProperty> getProperties() {
            return properties;
        }

        public String getId() {
            return id;
        }

        public String getUsername() {
            return username;
        }

        @Override
        public String toString() {
            return "User{" +
                    "properties=" + properties +
                    ", id='" + id + '\'' +
                    ", username='" + username + '\'' +
                    '}';
        }
    }

    public static class UserProperty {
        private final String name;
        private final String value;

        @JsonCreator
        public UserProperty(@JsonProperty("name") String name,
                            @JsonProperty("value") String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "UserProperty{" +
                    "name='" + name + '\'' +
                    ", value='" + value + '\'' +
                    '}';
        }
    }
}
